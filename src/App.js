import React from 'react';
import './App.css';
import {BrowserRouter as  Router, Route, Switch } from 'react-router-dom';
import Home from './Pages/Home';
import Contact from './Pages/Contact';
import About from './Pages/About';
import Navbar from './Component/navbar';
import Logo from './Component/Logo';
import Navigation from './Component/Navigation';
import Details from './Component/blogdetails';
import FooterPage from './Component/FooterPage';




function App() {
  return (
    <Router>
    <div className="App">
 
  <Navbar/>
  <Logo/>
  <Navigation/>
  <Switch>
  <Route path='/' exact component={Home}/>
  <Route path='/about'  component={About}/>
  <Route path='/contact'  component={Contact}/>
  <Route path='/blog/:id' component={Details}/>
  </Switch>
  <FooterPage/>

 

    </div>
    </Router>

  );
}

export default App;
