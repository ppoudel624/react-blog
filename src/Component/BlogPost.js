import React from 'react';
import '../Style/main.css'
function BlogPost(){
    return(
        <div className="blogpost" >
          <div className="blog_header">
          <span className="blogcategory">Featured</span>
          <h1 className="postTitle">Fitness mantra to live fit</h1>
          <span  className="postedBy">posted July 21, 2016</span>
       
          </div>
          <div className="imagecontainer">
            <img src={require('../assets/image/memories-from.jpg')} alt=""/>

          </div>


        </div>
      
    )
}
export default BlogPost;