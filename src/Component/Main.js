import React from 'react';
import '../Style/main.css'

function Main(props){
    return(
        <div className="main" >
            {props.children}
        </div>
      
    )
}
export default Main;