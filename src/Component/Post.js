import React from 'react';
import Main from '../Component/Main'
import BlogPost from '../Component/BlogPost';
import Sidebar from '../Component/Sidebar';



function Post(){
    return(
        <div >
            <Main>
            <BlogPost/>
                <Sidebar/>
            </Main>
     
        
        </div>
      
    )
}
export default Post;