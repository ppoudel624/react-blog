import React from 'react';
import Logo from './Logo';
import Card from './Card';
import Navigation from './Navigation';

function Section(){
    return(
        <div className="section">
           <Card>
               <Logo/>
               <Navigation/>

           </Card>
        
        </div>
      
    )
}
export default Section;