import React, { useState } from 'react';
import '../Style/main.css';
import {data} from'../Data/data';
import {Link} from 'react-router-dom';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import YouTubeIcon from '@material-ui/icons/YouTube';


function Sidebar(){
  
  const[posts,setPosts]=useState(data)
  console.log(posts);
 

    return(
        <div className="sidebar" >
          <div className="imagecontainer">
          <span>About us</span>
          <div className="profile" >
            <img src="https://upload.wikimedia.org/wikipedia/commons/e/ed/Elon_Musk_Royal_Society.jpg" alt=""/>
          </div>
          </div>
          <div style={{borderBottom:'1px solid grey',paddingLeft:'30%'}}>
          <div className="social">
          <p style={{color:'#0F90F2'}} className="facebok"><FacebookIcon/></p>
          <p style={{color:'#B32F8E'}} className="instagram"><InstagramIcon/></p>
          <p style={{color:'#1C9CEA'}} className="twitter"><TwitterIcon/></p>
          <p style={{color:'#FF0000'}} className="youtube"><YouTubeIcon/></p>
        
        </div>
          </div>
          <strong>Recent Posts</strong>
         
{
  posts.map(post=>
<div className="recent">

 <Link to={'/blog/'+post.id}> <h3>{post.blogTitle}</h3></Link>
  <span>{post.postedOn}</span>
</div>
  )
}




         
        </div>
        
      
    )
}
export default Sidebar;