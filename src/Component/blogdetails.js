import React from 'react';
import {data} from'../Data/data';
import Main from '../Component/Main'
import BlogPost from '../Component/BlogPost';
import Sidebar from '../Component/Sidebar';


function Details(props) {
    console.log(props)
    const blog=data.find(x=>x.id === props.match.params.id);
  return (
    <div >
       <Main>
       <div className="blogpost" >
          <div className="blog_header">
          <span className="blogcategory">Featured</span>
  <h1 className="postTitle">{blog.blogTitle}</h1>
  <span  className="postedBy">{blog.postedOn}</span>
       
          </div>
          <div className="imagecontainer">
            <img src={blog.image} alt=""/>
  <strong className="description">{blog.blogTitle}</strong>
  <br></br>
  <span>{blog.blogText}</span>

          </div>


        </div>
      
                <Sidebar/>
            </Main>
     
  
    </div>
  );
}

export default Details;
