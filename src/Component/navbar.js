import React from 'react';
import '../Style/home.css';
import {Link} from 'react-router-dom';
import FacebookIcon from '@material-ui/icons/Facebook';
import InstagramIcon from '@material-ui/icons/Instagram';
import TwitterIcon from '@material-ui/icons/Twitter';
import YouTubeIcon from '@material-ui/icons/YouTube';


function Navbar(){
    return(
        <div className="navbar">
        <nav className="navbar_nav">
           <Link to='/'> <a href="">Home</a></Link>
           <Link to='/about'> <a href="">About Us</a></Link>
             <Link to='/contact'><a href="">Contact Us</a></Link>
        </nav>
        <div className="social">
          <p style={{color:'#0F90F2'}} className="facebok"><FacebookIcon/></p>
          <p style={{color:'#B32F8E'}} className="instagram"><InstagramIcon/></p>
          <p style={{color:'#1C9CEA'}} className="twitter"><TwitterIcon/></p>
          <p style={{color:'#FF0000'}} className="youtube"><YouTubeIcon/></p>
        
        </div>
        </div>
        

    )
}
export default Navbar;